import path from 'path'

export const UPLOAD_IMAGES_FILE_DIR_TEMP = path.resolve('uploads/temp')
export const UPLOAD_IMAGES_FILE_DIR = path.resolve('uploads/')

// export const UPLOAD_VIDEO_FILE_DIR = path.resolve('uploads/temp')
export const UPLOAD_VIDEO_FILE_DIR = path.resolve('uploads/')
